<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Appointment Confirmation           _5b3155</name>
   <tag></tag>
   <elementGuidId>99f3d7cf-42c1-4f57-b03e-3020f5eb9041</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#summary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='summary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>78ac33b2-8a14-4e90-8d46-76bf4f0cbed3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>summary</value>
      <webElementGuid>c75364f1-9a07-48bb-8b29-d1a902b1402b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>section bg-primary</value>
      <webElementGuid>4a5c0252-23d2-4044-bfcb-50743036a23c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Hongkong CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/03/2023
                
            
            
                
                    Comment
                
                
                    Some Comment
                
            
            
                Go to Homepage
            
        
    
</value>
      <webElementGuid>f63cfb61-fa83-4ef5-86e4-cce65e678675</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;summary&quot;)</value>
      <webElementGuid>709ef8e0-efad-4342-9034-edbfaadd6fe3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//section[@id='summary']</value>
      <webElementGuid>519aa6ce-bd27-4ad3-8de0-ef0d78ececff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::section[1]</value>
      <webElementGuid>c6de2672-eb3a-4966-b2f7-f8edfb48c95a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::section[1]</value>
      <webElementGuid>571911ea-6699-4bf8-9161-2d1607eabd0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>e6e54cdd-e581-4f67-9c17-ff49cd9c1e9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[@id = 'summary' and (text() = '
    
        
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Hongkong CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/03/2023
                
            
            
                
                    Comment
                
                
                    Some Comment
                
            
            
                Go to Homepage
            
        
    
' or . = '
    
        
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Hongkong CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/03/2023
                
            
            
                
                    Comment
                
                
                    Some Comment
                
            
            
                Go to Homepage
            
        
    
')]</value>
      <webElementGuid>2fe90f41-7974-43e8-acc6-e332dc99c985</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[@id = 'summary' and (text() = '
    
        
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    No
                
            
            
                
                    Healthcare Program
                
                
                    Medicare
                
            
            
                
                    Visit Date
                
                
                    18/03/2023
                
            
            
                
                    Comment
                
                
                    Some Comment
                
            
            
                Go to Homepage
            
        
    
' or . = '
    
        
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    No
                
            
            
                
                    Healthcare Program
                
                
                    Medicare
                
            
            
                
                    Visit Date
                
                
                    18/03/2023
                
            
            
                
                    Comment
                
                
                    Some Comment
                
            
            
                Go to Homepage
            
        
    
')]</value>
      <webElementGuid>048d53e9-8333-4803-a766-1cb92d514894</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
