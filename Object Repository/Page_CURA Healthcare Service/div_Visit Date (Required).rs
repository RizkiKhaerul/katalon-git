<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Visit Date (Required)</name>
   <tag></tag>
   <elementGuidId>824c1942-e112-451f-8554-13f2044c32da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>84d7e8b9-45d7-4a1e-8aea-835fad9771e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group</value>
      <webElementGuid>4eeb300f-00dd-4aec-83b2-185ca5875843</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                </value>
      <webElementGuid>f4ecf8c2-2a2c-45b0-aa22-ac36dbdd347c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]</value>
      <webElementGuid>3522f175-6a5d-4eeb-a3ee-81074cde50da</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]</value>
      <webElementGuid>c9aef304-2ca3-47b5-853e-0b26306e2fe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[1]/preceding::div[4]</value>
      <webElementGuid>6245bb57-9084-452e-a7a6-44f6f1c3d690</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]</value>
      <webElementGuid>604bd187-158f-4120-8de0-121ab51d32bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                ' or . = '
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                ')]</value>
      <webElementGuid>8441ad4c-b6a1-45c9-b04f-0f96b7f66bbc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
