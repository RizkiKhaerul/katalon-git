<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Medicare                               _cb3988</name>
   <tag></tag>
   <elementGuidId>3885901f-75a7-4a0e-b71e-3892ed39a646</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b9d624a0-878d-4b73-a85f-f6b54479bbdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-4</value>
      <webElementGuid>c8edad4b-621b-45a5-afcf-29b394732a8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    </value>
      <webElementGuid>9ef224c1-1a1b-48df-9b77-49debd414ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-4&quot;]</value>
      <webElementGuid>3e822ea2-c2aa-47ba-ad3b-da0010af28df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div</value>
      <webElementGuid>28b62eef-9629-4ce7-943d-592b2e570ce9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/following::div[1]</value>
      <webElementGuid>e49f8627-5576-4f7a-b7c3-a19aa1a36e5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>2ab3e81f-0acd-4b10-8983-16e7c89bc3e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    ' or . = '
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    ')]</value>
      <webElementGuid>62ad0bf5-f937-4473-ae31-24a6d898b710</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
