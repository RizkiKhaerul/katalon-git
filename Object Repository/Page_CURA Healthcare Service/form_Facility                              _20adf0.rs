<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Facility                              _20adf0</name>
   <tag></tag>
   <elementGuidId>593ab2a4-fdca-47ee-bd30-c1d96bc421e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://katalon-demo-cura.herokuapp.com/appointment.php#summary']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>form.form-horizontal</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>fb17fef8-7d6f-4636-ab8a-bea28e64475f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-horizontal</value>
      <webElementGuid>cddebaaf-83ac-4bb2-b6d7-c698709105c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://katalon-demo-cura.herokuapp.com/appointment.php#summary</value>
      <webElementGuid>f5b2e077-4f4c-4a9f-ba9a-63afa0377d03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>a0827762-ba20-4c62-b9db-25822cc1ea21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            </value>
      <webElementGuid>1536189c-b0e7-424a-8efd-1568b090246a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]</value>
      <webElementGuid>0b954651-56ba-40df-9f18-38d1e11ae3f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://katalon-demo-cura.herokuapp.com/appointment.php#summary']</value>
      <webElementGuid>c1d6f379-54a8-4922-a6fe-f963bae16f5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form</value>
      <webElementGuid>d8284f1e-7c7e-4abf-b8e4-5dbdaa39afcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[2]/following::form[1]</value>
      <webElementGuid>5b0e5e4c-c9e6-4ec8-89e4-40c58a6ebfed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::form[1]</value>
      <webElementGuid>62580c4f-d0a3-4eea-be4e-adf021770178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>929d026b-ec10-4c6d-a8df-7206587a0692</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            ' or . = '
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            ')]</value>
      <webElementGuid>5a78dd6c-bc73-46dc-8def-25aa09156eb8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
