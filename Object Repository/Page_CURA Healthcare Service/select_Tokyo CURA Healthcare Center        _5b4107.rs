<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Tokyo CURA Healthcare Center        _5b4107</name>
   <tag></tag>
   <elementGuidId>c2c79b76-e1a1-4e04-9d28-45482fed094d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#combo_facility</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='combo_facility']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b984590e-821a-425a-a56d-7c9a627df668</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>combo_facility</value>
      <webElementGuid>d14feb4c-5ceb-409e-b77d-f0433c819537</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>facility</value>
      <webElementGuid>f6e6fc37-57c4-42db-93f8-57414b5489f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>aac35e32-bef7-4616-8ed5-d82d61852c56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        </value>
      <webElementGuid>4f0bd78a-db91-4816-9c88-898f56accad1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;combo_facility&quot;)</value>
      <webElementGuid>26947e28-ae05-4244-a72a-03ef7c11c0e0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='combo_facility']</value>
      <webElementGuid>5d32f584-b514-4d7b-9004-42bed570ffd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div/div/select</value>
      <webElementGuid>e79b04ea-ee09-43e9-8e62-c716cf90657c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/following::select[1]</value>
      <webElementGuid>44f4ca8e-0134-4736-8290-444403bf9bb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[2]/following::select[1]</value>
      <webElementGuid>2ca0d94f-a917-4a6b-a355-dfd420a1e119</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/preceding::select[1]</value>
      <webElementGuid>23956f7a-fe76-4276-a6db-426c3319083a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>4afd63dc-4a43-4130-bb1d-ea75f2c525c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'combo_facility' and @name = 'facility' and (text() = '
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        ' or . = '
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        ')]</value>
      <webElementGuid>b1c019ea-6454-409c-87ba-84d6ec0a6b3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
