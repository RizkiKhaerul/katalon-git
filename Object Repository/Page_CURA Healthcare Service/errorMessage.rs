<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>errorMessage</name>
   <tag></tag>
   <elementGuidId>572f209f-4c59-49b1-9305-bf7adf412e80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(text(),'Please fill out this field.')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[contains(text(),'Please fill out this field.')]</value>
      <webElementGuid>3f64f625-4bd9-49cb-a5a1-4e49277ac08e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
