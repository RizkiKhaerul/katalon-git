<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_(678) 813-1KMS</name>
   <tag></tag>
   <elementGuidId>196200a1-7a33-459b-bce4-671d1abb286b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare Service'])[3]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.list-unstyled > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>d1209cf2-02df-4682-9ecf-bf7733f8c224</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> (678) 813-1KMS</value>
      <webElementGuid>565fe8a9-4813-4382-a4a5-2ce0f19f5e4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/footer[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-10 col-lg-offset-1 text-center&quot;]/ul[@class=&quot;list-unstyled&quot;]/li[1]</value>
      <webElementGuid>d6a469bc-580f-4985-8bc5-7a8a6862ad46</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare Service'])[3]/following::li[1]</value>
      <webElementGuid>493ba9b6-d7d4-4940-b182-b31d1af5fb2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[3]/following::li[1]</value>
      <webElementGuid>33edf84e-5c10-4618-8367-5d96bfe0524e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='info@katalon.com'])[1]/preceding::li[1]</value>
      <webElementGuid>1ad71864-c29c-4911-9e62-eeba94c4a9f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='(678) 813-1KMS']/parent::*</value>
      <webElementGuid>1fb7bc86-df0d-4fb7-bc9a-673ee18b8ba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/ul/li</value>
      <webElementGuid>f5f88491-30d6-4610-bc15-74fd0cbed106</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' (678) 813-1KMS' or . = ' (678) 813-1KMS')]</value>
      <webElementGuid>b49a41f2-2ccd-4974-8a5c-db9b1e458994</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
